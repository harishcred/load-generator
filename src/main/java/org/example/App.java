package org.example;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import io.gitbub.devlibx.easy.helper.json.JsonUtils;
import io.gitbub.devlibx.easy.helper.map.StringObjectMap;
import net.openhft.chronicle.queue.ExcerptAppender;
import net.openhft.chronicle.queue.ExcerptTailer;
import net.openhft.chronicle.queue.impl.single.SingleChronicleQueue;
import net.openhft.chronicle.queue.impl.single.SingleChronicleQueueBuilder;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static String input = "{\n" +
            "    \"store\": {\n" +
            "        \"book\": [\n" +
            "            {\n" +
            "                \"category\": \"reference\",\n" +
            "                \"author\": \"Nigel Rees\",\n" +
            "                \"title\": \"Sayings of the Century\",\n" +
            "                \"price\": 8.95\n" +
            "            },\n" +
            "            {\n" +
            "                \"category\": \"fiction\",\n" +
            "                \"author\": \"Evelyn Waugh\",\n" +
            "                \"title\": \"Sword of Honour\",\n" +
            "                \"price\": 12.99\n" +
            "            },\n" +
            "            {\n" +
            "                \"category\": \"fiction\",\n" +
            "                \"author\": \"Herman Melville\",\n" +
            "                \"title\": \"Moby Dick\",\n" +
            "                \"isbn\": \"0-553-21311-3\",\n" +
            "                \"price\": 8.99\n" +
            "            },\n" +
            "            {\n" +
            "                \"category\": \"fiction\",\n" +
            "                \"author\": \"J. R. R. Tolkien\",\n" +
            "                \"title\": \"The Lord of the Rings\",\n" +
            "                \"isbn\": \"0-395-19395-8\",\n" +
            "                \"price\": 22.99\n" +
            "            }\n" +
            "        ],\n" +
            "        \"bicycle\": {\n" +
            "            \"color\": \"red\",\n" +
            "            \"price\": 19.95\n" +
            "        }\n" +
            "    },\n" +
            "    \"expensive\": 10\n" +
            "}";

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(input);
        for (int i = 0; i < 1000; i++) {
            String author0 = JsonPath.read(document, "$.store.book[0].author");
        }
        System.out.println(System.currentTimeMillis() - start);


        String JSON =
                "{\"widget\": {\n" +
                        "    \"debug\": \"on\",\n" +
                        "    \"window\": {\n" +
                        "        \"title\": \"Sample Konfabulator Widget\",\n" +
                        "        \"name\": \"main_window\",\n" +
                        "        \"width\": 500,\n" +
                        "        \"height\": 500\n" +
                        "    },\n" +
                        "    \"image\": { \n" +
                        "        \"src\": \"Images/Sun.png\",\n" +
                        "        \"name\": \"sun1\",\n" +
                        "        \"hOffset\": 250,\n" +
                        "        \"vOffset\": 250,\n" +
                        "        \"alignment\": \"center\"\n" +
                        "    },\n" +
                        "    \"text\": {\n" +
                        "        \"data\": \"Click Here\",\n" +
                        "        \"size\": 36,\n" +
                        "        \"style\": \"bold\",\n" +
                        "        \"name\": \"text1\",\n" +
                        "        \"hOffset\": 250,\n" +
                        "        \"vOffset\": 100,\n" +
                        "        \"alignment\": \"center\",\n" +
                        "        \"onMouseUp\": \"sun1.opacity = (sun1.opacity / 100) * 90;\"\n" +
                        "    }\n" +
                        "}}    \n";

        start = System.currentTimeMillis();
        StringObjectMap sm = JsonUtils.convertAsStringObjectMap(JSON);
        System.out.println("First time=" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        sm = JsonUtils.convertAsStringObjectMap(JSON);
        System.out.println("Second time=" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        sm = JsonUtils.convertAsStringObjectMap(JSON);
        for (int i = 0; i < 200; i++) {
            String s = sm.path("widget.window.title", String.class);
        }
        System.out.println(System.currentTimeMillis() - start);
    }

    public static void _main(String[] args) throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                SingleChronicleQueue queue = SingleChronicleQueueBuilder.builder().path(".").build();
                ExcerptAppender appender = queue.acquireAppender();
                for (int i = 0; i < 100; i++) {
                    appender.writeText("hello word " + i);
                }
                long end = System.currentTimeMillis();
                System.out.println("Time Taken = " + (end - start));
            }
        });//.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                SingleChronicleQueue queue = SingleChronicleQueueBuilder.builder().path(".").build();
                ExcerptTailer tailer = queue.createTailer();
                tailer.moveToIndex(80934363725923L);
                for (int i = 0; i < 110; i++) {
                    System.out.println(tailer.readText() + " " + tailer.index());
                }
            }
        }).start();

        Thread.sleep(1000000);
    }
}
