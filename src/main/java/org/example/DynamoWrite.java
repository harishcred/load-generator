package org.example;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.google.common.util.concurrent.RateLimiter;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;


public class DynamoWrite {
    public static String tableName = "delete_test_2";
    public static final long MAX = 5000;

    public static void main(String[] args) throws InterruptedException {
        BasicSessionCredentials bc = new BasicSessionCredentials(
                "ASIA525GDTDNIMM6EAGY",
                "gUbIkoPTOsVTXnERZmGcCW20UGa43wyt3qxRPY7a",
                "FwoGZXIvYXdzEPn//////////wEaDFA5/Jf9uC2TNSzXoCL4AbRzGwI7g9nY9v8gjmltRUqXaeTn/uI8bnIVLrN8S45vGjjRr+kLddT52Lsm9so2jizwDhDZwsJyHBfb0LxjiTufvklbI0e+4CSf/nlEGDsSj7MZhh14Et3fIa/YL3krLwVpBVucd5eKqocj8fHDux72dRUBvD38Qpxi4FWzHioUzRZqv6u3lMyNqygMwn2HYhtJn9q1cnhlnRRr5ACf3gl9Gaj0oX2cL6czFDqGKsxo9cvB01aY6u50f5eIKR76qQKMWQq12MaXsqnfbZASmz95eKgZA7yqqwErX+hqSQJuHEbHeT3BnNrp7KZlvVnlkoy4fnwByIz6KNTO2IgGMisoDMHmxds5KV+RrwCO7/b6m/RxjVzmeEu/7UDyvouMw1vaF1A2XNCCSM6A"
        );
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder
                .standard()
                .withClientConfiguration(new ClientConfiguration().withClientExecutionTimeout(2000))
                .withRegion(Regions.AP_SOUTH_1)
                // .withCredentials(new AWSStaticCredentialsProvider(bc))
                .build();
        DynamoDB dynamoDB = new DynamoDB(client);


        Table table = dynamoDB.getTable(tableName);

        AtomicInteger a = new AtomicInteger();
        for (int j = 0; j < 0; j++) {
            new Thread(() -> {
                for (int i = 0; i < 1_000_000; i++) {
                    try {
                        putData(table, a.incrementAndGet());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        rateLimiter(table, client);
        Flux<Item> stream = get(table);
        AtomicInteger retry = new AtomicInteger();
        for (int j = 0; j < 250; j++) {
            new Thread(() -> {
                stream.subscribe(new Subscriber<Item>() {
                    private Subscription s;

                    @Override
                    public void onSubscribe(Subscription subscription) {
                        this.s = subscription;
                        subscription.request(1);
                    }

                    @Override
                    public void onNext(Item item) {
                        try {
                            while (true) {
                                // System.out.println(item);
                                try {

                                    PutItemOutcome outcome = table.putItem(item);
                                } catch (Exception e) {
                                    if (retry.incrementAndGet() % 100 == 0) {
                                        System.out.println("doing retry for item - " + item + " error" + e.getMessage());
                                    }
                                    try {
                                        // Thread.sleep(1);
                                    } catch (Exception ignored) {
                                    }
                                    continue;
                                }
                                break;
                            }
                        } finally {
                            s.request(1);
                        }

                        int index = mainCounter.get();
                        if (index % 100 == 0) {
                            System.out.println("Index = " + index + " " + rateLimiterObj.getRate() + " time = " + ((System.currentTimeMillis() - start) / 1000));
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println("Got error - " + throwable);
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("got completes");
                    }
                });
            }).start();
        }

        Thread.sleep(100000000);
    }

    static AtomicInteger mainCounter = new AtomicInteger();
    static RateLimiter rateLimiterObj = RateLimiter.create(10);
    static boolean done = false;
    static long start = System.currentTimeMillis();

    static void rateLimiter(Table table, AmazonDynamoDB client) {
        new Thread(() -> {
            while (true) {
                try {
                    TableDescription table_info = client.describeTable(tableName).getTable();
                    long write = table_info.getProvisionedThroughput().getWriteCapacityUnits();
                    System.out.println("----->>>> rate = " + write);
                    rateLimiterObj.setRate(write);
                    // System.out.println(table_info + " " + table.getTableName());
                    //
                    // ProvisionedThroughputDescription r = table_info.getProvisionedThroughput();
                    // r.setWriteCapacityUnits(1000L);
                    // table_info.setProvisionedThroughput(r);
                    System.out.println("----->>>> setting rate = " + rateLimiterObj.getRate());
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                }
            }
        }).start();
    }

    public static Flux<Item> get(Table table) {


        return Flux.create(new Consumer<FluxSink<Item>>() {
            @Override
            public void accept(FluxSink<Item> stringFluxSink) {
                for (int i = 0; i < 1_000_000; i++) {
                    try {
                        int index = mainCounter.incrementAndGet();
                        Item item = new Item()
                                .withPrimaryKey("id", "" + index)
                                .withString("Title", "Bicycle 123 " + index);
                        // rateLimiterObj.acquire();
                        stringFluxSink.next(item);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, FluxSink.OverflowStrategy.BUFFER);
    }


    public static void putData(Table table, int index) {
        Item item = new Item()
                .withPrimaryKey("id", "" + index)
                .withString("Title", "Bicycle 123 " + index);
        PutItemOutcome outcome = table.putItem(item);
        if (index % 1000 == 0) {
            System.out.println(outcome + "index" + index + " " + (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();
        }
    }
}
