package org.example;

import com.google.common.util.concurrent.RateLimiter;
import org.joda.time.DateTime;

import java.util.concurrent.atomic.AtomicInteger;


interface Ticker {
    int tick();
}

class MinTicker implements Ticker {
    DateTime start;

    public MinTicker() {
        start = DateTime.now();
    }

    @Override
    public int tick() {
        long first = start.getMillis();
        long second = DateTime.now().getMillis();
        int delta = (int) (second - first);
        // return delta / 60_000;
        return delta / 4_000;
    }
}

@SuppressWarnings("UnstableApiUsage")
public class R {
    static int afterSec = 10;
    static int maxPerSec = 100;
    static int maxPerMinute = maxPerSec * 60;
    static DateTime now = DateTime.now();
    static DateTime afterMinute = now.plusSeconds(afterSec);
    static RateLimiter rateLimiter = RateLimiter.create(maxPerSec);

    public static void main(String[] args) throws InterruptedException {
        Ticker ticker = new MinTicker();
        RateLimiter rateLimiter = RateLimiter.create(100);

        Object lock = new Object();
        AtomicInteger counter = new AtomicInteger();
        AtomicInteger currentTick = new AtomicInteger(0);
        for (int i = 0; i < 1; i++) {
            new Thread(() -> {
                while (true) {
                    rateLimiter.acquire();
                    int tick = ticker.tick();
                    if (currentTick.get() == tick - 1) {
                        synchronized (lock) {
                            currentTick.incrementAndGet();
                            double currentRate = rateLimiter.getRate();
                            double newRate = currentRate * 2;
                            System.out.println(currentRate + " " + newRate);
                            rateLimiter.setRate(newRate);
                        }
                    }
                    try {
                        Thread.sleep(5);
                    } catch (Exception e) {
                    }
                }
            }).start();
        }
        Thread.sleep(1000000);
    }

    public static void __main(String[] args) throws InterruptedException {

        Object lock = new Object();
        AtomicInteger counter = new AtomicInteger();

        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                while (true) {
                    synchronized (lock) {
                        rateLimiter.acquire();
                        if (DateTime.now().isAfter(afterMinute)) {
                            now = DateTime.now();
                            afterMinute = now.plusSeconds(afterSec);
                            maxPerSec = maxPerSec * 2;
                            System.out.println("Double the rate " + maxPerSec);
                            rateLimiter.setRate(maxPerSec);
                        }
                    }
                    counter.incrementAndGet();
                    if (counter.get() % 100 == 0) {
                        System.out.println(counter.get() + "   " + rateLimiter.getRate());
                    }
                }
            }).start();
        }


        Thread.sleep(1000000);
    }
}
