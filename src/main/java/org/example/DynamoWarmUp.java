package org.example;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.google.common.base.Strings;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class DynamoWarmUp {
    private static AmazonDynamoDB client;
    private static AtomicInteger recordCounter;
    private static String awsAccessKey;
    private static String awsSecretKey;
    private static String sessionToken;
    private static String table;
    private static int recordCount;
    private static int threads;
    private static int timeout;
    private static int maxTimeToRun;
    private static DynamoDB dynamoDB;
    private static Table dynamoTable;
    private static int testUserCount;

    public static void main(String[] args) throws ParseException, InterruptedException {
        Options options = new Options();
        options.addOption(new Option("awsAccessKey", "awsAccessKey", true, "awsAccessKey"));
        options.addOption(new Option("awsSecretKey", "awsSecretKey", true, "awsSecretKey"));
        options.addOption(new Option("sessionToken", "sessionToken", true, "sessionToken"));
        options.addOption(new Option("table", "table", true, "table"));
        options.addOption(new Option("recordCount", "recordCount", true, "hom many records to insert - we will insert N records to testUserCount users"));
        options.addOption(new Option("thread", "thread", true, "how man threads to be used to generate the load"));
        options.addOption(new Option("timeout", "timeout", true, "timeout for AWS dynamo client to write each row"));
        options.addOption(new Option("timeToRunInMin", "timeToRunInMin", true, "max time the program will run - after that it will exit"));
        options.addOption(new Option("testUserCount", "testUserCount", true, "how many dummy users to add"));


        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        awsAccessKey = cmd.getOptionValue("awsAccessKey", "");
        awsSecretKey = cmd.getOptionValue("awsSecretKey", "");
        sessionToken = cmd.getOptionValue("sessionToken", "");
        table = cmd.getOptionValue("table", "uas_entities");
        recordCount = Integer.parseInt(cmd.getOptionValue("recordCount", "2000000"));
        threads = Integer.parseInt(cmd.getOptionValue("thread", "100"));
        timeout = Integer.parseInt(cmd.getOptionValue("timeout", "1000"));
        testUserCount = Integer.parseInt(cmd.getOptionValue("testUserCount", "1000"));
        recordCounter = new AtomicInteger(recordCount);

        System.out.println("awsAccessKey=" + awsAccessKey);
        System.out.println("awsSecretKey=" + awsSecretKey);
        System.out.println("sessionToken=" + sessionToken);
        System.out.println("table=" + table);
        System.out.println("recordCount=" + recordCount);
        System.out.println("thread=" + threads);
        System.out.println("testUserCount=" + testUserCount);


        if (!Strings.isNullOrEmpty(awsAccessKey)) {
            BasicSessionCredentials bc = new BasicSessionCredentials(
                    awsAccessKey,
                    awsSecretKey,
                    sessionToken
            );
            client = AmazonDynamoDBClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(bc))
                    .withClientConfiguration(new ClientConfiguration().withClientExecutionTimeout(timeout))
                    .withRegion(Regions.AP_SOUTH_1)
                    .build();
        } else {
            client = AmazonDynamoDBClientBuilder
                    .standard()
                    .withClientConfiguration(new ClientConfiguration().withClientExecutionTimeout(timeout))
                    .withRegion(Regions.AP_SOUTH_1)
                    .build();
        }
        dynamoDB = new DynamoDB(client);
        dynamoTable = dynamoDB.getTable(table);

        Flux<Item> stream = itemGenerator(dynamoTable);

        AtomicInteger retry = new AtomicInteger();
        CountDownLatch countDownLatch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            new Thread(() -> {
                stream.subscribe(new Subscriber<Item>() {
                    private Subscription subscription;

                    @Override
                    public void onSubscribe(Subscription subscription) {
                        this.subscription = subscription;
                        subscription.request(1);
                    }

                    @Override
                    public void onNext(Item item) {
                        try {
                            while (true) {
                                try {
                                    PutItemOutcome outcome = dynamoTable.putItem(item);
                                } catch (Exception e) {
                                    if (retry.incrementAndGet() % 100 == 0) {
                                        System.out.println("doing retry for item - " + item + " error" + e.getMessage());
                                    }
                                    continue;
                                }
                                break;
                            }
                        } finally {
                            subscription.request(1);
                            recordCounter.decrementAndGet();
                            int c = recordCounter.get();
                            if (c % 1000 == 0) {
                                System.out.println("Remaining records = " + c);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                        recordCounter.decrementAndGet();
                    }

                    @Override
                    public void onComplete() {
                        countDownLatch.countDown();
                    }
                });
            }).start();
        }

        printWriteCapacityUnits();

        boolean result = countDownLatch.await(maxTimeToRun, TimeUnit.MINUTES);
        if (result) {
            System.out.println("Exiting from program");
        } else {
            System.out.println("Exiting from program - timeout");
        }
    }


    static void printWriteCapacityUnits() {
        new Thread(() -> {
            while (recordCounter.get() > 0) {
                try {
                    TableDescription table_info = client.describeTable(table).getTable();
                    long write = table_info.getProvisionedThroughput().getWriteCapacityUnits();
                    System.out.println("Current WriteCapacityUnits from DynamoDB table = " + write);
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static Flux<Item> itemGenerator(Table table) {
        return Flux.create(stringFluxSink -> {
            int i = 0;
            while (recordCounter.get() > 0) {
                try {
                    String userId = "dummy_" + (i++);
                    Item item = new Item()
                            .withPrimaryKey("id", userId, "namespace", "*")
                            .withString("ignore", "data_" + userId);
                    stringFluxSink.next(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (i > testUserCount) {
                    i = 0;
                }
            }
            stringFluxSink.complete();
        }, FluxSink.OverflowStrategy.BUFFER);
    }
}
